** Settings **
Library    SeleniumLibrary
    
** Test Cases **
Ouvrir navigateur
    ouvrir chrome
    Set Browser Implicit wait    5 
    sleep    2
    CLose Browser
Go To linkedIn
    ouvrir linkedin
    Click link    href="https://www.linkedin.com/signup/cold-join?trk=guest_homepage-basic_nav-header-join"
recherche google
    ouvrir chrome
    Input Text    name=q    Formation FMFP
    Press Keys    None    ENTER
    
  
** Variable **
${URL_GOOGLE}    https://google.com
${URL_LINKEDIN}    https://fr.linkedin.com/
${NAVIGATEUR}    Chrome

** Keywords***
ouvrir linkedin
    Open Browser    ${URL_LINKEDIN}    Chrome
ouvrir chrome
    Open Browser    ${URL_GOOGLE}    Chrome